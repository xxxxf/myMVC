package com.xxxxf.mvc.demo.service.impl;

import com.xxxxf.mvc.demo.service.INamedService;
import com.xxxxf.mvc.demo.service.IService;
import com.xxxxf.mvc.framework.annotation.MyAutoWired;
import com.xxxxf.mvc.framework.annotation.MyService;

/**
 * @author:xxxxf
 */
@MyService("myName")
public class NamedServiceImpl implements INamedService {
    @MyAutoWired
    IService iService;
}
