package com.xxxxf.mvc.demo.controller;

import com.xxxxf.mvc.demo.service.INamedService;
import com.xxxxf.mvc.demo.service.IService;
import com.xxxxf.mvc.framework.annotation.MyAutoWired;
import com.xxxxf.mvc.framework.annotation.MyController;
import com.xxxxf.mvc.framework.annotation.MyRequestMapping;
import com.xxxxf.mvc.framework.annotation.MyRequestParam;
import com.xxxxf.mvc.framework.servlet.MyModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author:xxxxf
 */
@MyController
@MyRequestMapping("/web")
public class FirstController {
    @MyAutoWired
    private IService service;
    @MyAutoWired("myName")
    private INamedService namedService;

    @MyRequestMapping("/query/.*.json")
    public MyModelAndView query(HttpServletRequest request, HttpServletResponse response,
                                @MyRequestParam(value = "name",required = false) String name,
                                @MyRequestParam("address")String address){
        Map<String,Object> model = new HashMap<String, Object>();
        model.put("name",name);
        model.put("address",address);
        return new MyModelAndView("first.pgml",model);

    }

    @MyRequestMapping("/add.json")
    public MyModelAndView add(HttpServletRequest request, HttpServletResponse response){
        out(response,"this is json string");
        return null;
    }
    public void out(HttpServletResponse response,String str){
        try {
            response.getWriter().write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
