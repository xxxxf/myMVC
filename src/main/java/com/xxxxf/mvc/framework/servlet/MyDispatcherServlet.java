package com.xxxxf.mvc.framework.servlet;

import com.xxxxf.mvc.framework.annotation.MyController;
import com.xxxxf.mvc.framework.annotation.MyRequestMapping;
import com.xxxxf.mvc.framework.annotation.MyRequestParam;
import com.xxxxf.mvc.framework.context.MyApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author:xxxxf
 */
public class MyDispatcherServlet extends HttpServlet {
    private static final String LOCATION = "contextConfigLocation";
    private List<Handler> handlerMapping = new ArrayList<Handler>();
    private Map<Handler,HandlerAdapter> adapterMapping = new HashMap<Handler, HandlerAdapter>();
    private List<ViewResolver> viewResolvers = new ArrayList<ViewResolver>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
//  在这里调用自己写的Controller方法
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            doDispatch(req,resp);
        }catch (Exception e){
            resp.getWriter().write("500 Exception,Msg"+Arrays.toString(e.getStackTrace()));
        }
    }

    /**
     * 初始化IOC容器
     * */
    @Override
    public void init(ServletConfig config) throws ServletException {
//        IOC 容器必须要初始化
//        这里假设容器已经启动
        try {
            MyApplicationContext context = new MyApplicationContext(config.getInitParameter(LOCATION));
//            1、请求解析
              initMultipartResolver(context);
//            2、多语言、国际化
              initLocaleResolver(context);
//            3、主题view层
              initThemeResolver(context);
//            ==========重要===========
//            4、解析url和method的关联关系
              initHandlerMappings(context);
//            5、适配器（匹配的过程）
              initHandlerAdapters(context);
//            ==========重要===========
//            6、异常解析
              initHandlerExceptionResolvers(context);
//            7、视图转发（根据师徒名字匹配到一个具体模板）
              initRequestToViewNameTranslator(context);
//            8、解析模板中的内容（拿到服务器传过来的数据，生成HTML代码）
              initViewResolvers(context);
              initFlashMapManager(context);
              System.out.println("MySpring MVC is init.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    请求解析
    private void initMultipartResolver(MyApplicationContext context){}
//    多语言、国际化
    private void initLocaleResolver(MyApplicationContext context){}
//    主题View层
    private void initThemeResolver(MyApplicationContext context){}
//    解析url 和 method的关联关系
    private void initHandlerMappings(MyApplicationContext context){
        Map<String,Object> ioc = context.getAll();
        if (ioc.isEmpty()){return;}
//        只要是由Controller修改类，里面方法全部找出来
//        而且这个方法应该是要加了RequestMapping注解，如果没加这个注解，这个方法是不能被外界访问
        for (Map.Entry<String,Object> entry : ioc.entrySet()){
            Class<?> clazz = entry.getValue().getClass();
            if (!clazz.isAnnotationPresent(MyController.class)){continue;}
            String url = "";
            if (clazz.isAnnotationPresent(MyRequestMapping.class)){
                MyRequestMapping requestMapping = clazz.getAnnotation(MyRequestMapping.class);
                url = requestMapping.value();
            }
//            扫描Controller下面的所有的方法
            Method[] methods = clazz.getMethods();
            for (Method method : methods){
                if (!method.isAnnotationPresent(MyRequestMapping.class)){continue;}
                MyRequestMapping requestMapping = method.getAnnotation(MyRequestMapping.class);
                String regex = (url + requestMapping.value()).replaceAll("/+","/");
                Pattern pattern = Pattern.compile(regex);
                handlerMapping.add(new Handler(pattern,entry.getValue(),method));
                System.out.println("Mapping:"+regex+" "+method.toString());
            }
        }
    }
//    适配器（匹配的过程）
//    主要是用来动态匹配我们的参数
    private void initHandlerAdapters(MyApplicationContext context){
        if (handlerMapping.isEmpty()){return;}
        //参数作为key，参数的索引号作为值
        Map<String,Integer> paramMapping = new HashMap<String, Integer>();
//        只需要取出具体的某个方法
        for(Handler handler : handlerMapping){
            //把这个方法上面所有的参数全部获取到
            Class<?>[] paramsTypes = handler.method.getParameterTypes();
            //有顺序，但是通过反射，没法拿到参数名字
            //匹配自定义参数列表
            for (int i = 0;i < paramsTypes.length;i++){
                Class<?> type = paramsTypes[i];
                if (type == HttpServletRequest.class ||
                type == HttpServletResponse.class){
                    paramMapping.put(type.getName(),i);
                }
            }
//            这里是匹配Request和Response
            Annotation[][] pa = handler.method.getParameterAnnotations();
            for (int i = 0;i < pa.length;i++){
                for (Annotation a : pa[i]){
                    if (a instanceof MyRequestParam){
                        String paramName = ((MyRequestParam) a).value();
                        if (!"".equals(paramName.trim())){
                            paramMapping.put(paramName,i);
                        }
                    }
                }
            }
            adapterMapping.put(handler,new HandlerAdapter(paramMapping));
        }
    }
//    异常解析
    private void initHandlerExceptionResolvers(MyApplicationContext context){}
//    视图转发（根据视图名字匹配到一个具体模板）
    private void initRequestToViewNameTranslator(MyApplicationContext context){}
//    解析模板中的内容（拿到服务器传过来的数据，生成HTML代码）
    private void initViewResolvers(MyApplicationContext context){
        //模板一般是不会存到WebRoot/Webapp下的，而是放在web-inf下或classes下
        //这样就避免了用户直接请求到模板
        //加载模板的个数，存储到缓存中
        //检查模板中的语法错误
        String tempateRoot = context.getConfig().getProperty("templateRoot");
        //归根到底就是一个文件，普通文件
        String rootPath = this.getClass().getClassLoader().getResource(tempateRoot).getFile();
        File rootDir = new File(rootPath);
        for (File template : rootDir.listFiles()){
            viewResolvers.add(new ViewResolver(template.getName(),template));
        }
    }
    private void initFlashMapManager(MyApplicationContext context){}
    private void doDispatch(HttpServletRequest req,HttpServletResponse resp){
        try {
            //        先取出来一个Handler,从HandlerMapping取
            Handler handler = getHandler(req);
            if (handler == null) {
                resp.getWriter().write("404 Not Found");
                return;
            }
//        再取出来一个适配器
//        再由适配器去调用我们具体的方法
            HandlerAdapter ha = getHandlerAdapter(handler);
            MyModelAndView mv = ha.handle(req, resp, handler);
//        写一个自己的模板框架
            applyDefaultViewName(resp,mv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyDefaultViewName(HttpServletResponse resp,MyModelAndView mv) throws Exception {
        if (null == mv ){return;}
        if (viewResolvers.isEmpty()){return;}
        for (ViewResolver resolver : viewResolvers){
            if (!mv.getView().equals(resolver.getViewName())){continue;}
            String r = resolver.parse(mv);
            if (r != null){
                resp.getWriter().write(r);
                break;
            }
        }
    }

    private class Handler{
        protected Object controller;
        protected Method method;
        protected Pattern pattern;

        protected Handler(Pattern pattern,Object controller,Method method){
            this.pattern = pattern;
            this.controller = controller;
            this.method = method;
        }
    }

    private Handler getHandler(HttpServletRequest req){
        //循环handlerMapping
        if (handlerMapping.isEmpty()){
            return null;
        }
        String url =req.getRequestURI();
        String contextPath = req.getContextPath();
        url = url.replace(contextPath,"").replaceAll("/+","/");

        for (Handler handler : handlerMapping){
            Matcher matcher = handler.pattern.matcher(url);
            if (!matcher.matches()){continue;}
            return handler;
        }
        return null;
    }

    private HandlerAdapter getHandlerAdapter(Handler handler){
        if (adapterMapping.isEmpty()){return null;}
        return adapterMapping.get(handler);
    }
    /**
     * 方法适配器
     * */
    private class HandlerAdapter {
        private Map<String, Integer> paramMapping;

        public HandlerAdapter(Map<String, Integer> paramMapping) {
            this.paramMapping = paramMapping;
        }

        //    主要目的是用反射调用url对应的method
        public MyModelAndView handle(HttpServletRequest req, HttpServletResponse resp, Handler handler) throws Exception {
//        为什么要传req、为什么要传resp、为什么传handler
            Class<?>[] paramTypes = handler.method.getParameterTypes();
            //要想给参数赋值，只能通过索引号来找到具体的某个参数
            Object[] paramValues = new Object[paramTypes.length];
            Map<String, String[]> params = req.getParameterMap();
            for (Map.Entry<String, String[]> param : params.entrySet()) {
                String value = Arrays.toString(param.getValue()).replaceAll("\\[|\\]", "").replaceAll(",\\s", ",");
                if (!this.paramMapping.containsKey(param.getKey())){continue;}
                int index = this.paramMapping.get(param.getKey());
                //单个赋值是不行的
                paramValues[index] = castStringValues(value,paramTypes[index]);
            }
            //request 和 response 要赋值
            String reqName = HttpServletRequest.class.getName();
            if (this.paramMapping.containsKey(reqName)){
                int reqIndex = this.paramMapping.get(reqName);
                paramValues[reqIndex] = req;
            }
            String respName = HttpServletResponse.class.getName();
            if (this.paramMapping.containsKey(respName)){
                int respIndex = this.paramMapping.get(respName);
                paramValues[respIndex] = resp;
            }

            boolean isModelAndView = handler.method.getReturnType() == MyModelAndView.class;
            Object r = handler.method.invoke(handler.controller,paramValues);
            if (isModelAndView){
                return (MyModelAndView) r;
            }else {
                return null;
            }

        }
        private Object castStringValues(String value,Class<?> clazz){
            if (clazz == String.class){
                return value;
            }else if (clazz == Integer.class){
                return Integer.valueOf(value);
            }else if (clazz == int.class){
                return Integer.valueOf(value).intValue();
            }else {
                return null;
            }
        }
    }

    private class ViewResolver{
        private String viewName;
        private File file;

        protected ViewResolver(String viewName,File file){
            this.file = file;
            this.viewName = viewName;
        }

        protected String parse(MyModelAndView mv) throws Exception {

            StringBuffer sb = new StringBuffer();
            RandomAccessFile ra = new RandomAccessFile(this.file,"r");
            try{
//            模板框架的语法非常复杂，但是原理是一样的
//            无非都是使用正则表达式来处理字符串而已
                String line = null;
                while (null != (line = ra.readLine())){
                    Matcher m = matcher(line);
                    while (m.find()){
                        for (int i = 1;i <= m.groupCount(); i++){
                            String paramName = m.group(i);
                            Object paramValue = mv.getModel().get(paramName);
                            if (null == paramValue){continue;}
                            line = line.replaceAll("@\\{"+paramName+"\\}",paramValue.toString());
                        }
                    }
                    sb.append(line);
                }
            }finally {
                    ra.close();
            }
            return sb.toString();
        }
        private Matcher matcher(String str){
            Pattern pattern = Pattern.compile("@\\{(.+?)\\}",Pattern.CASE_INSENSITIVE);
            Matcher m = pattern.matcher(str);
            return m;
        }
        public String getViewName(){
            return viewName;
        }
    }

}
