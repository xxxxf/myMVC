package com.xxxxf.mvc.framework.annotation;

import java.lang.annotation.*;

/**
 * @author:xxxxf
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAutoWired {
    String value() default "";
}
