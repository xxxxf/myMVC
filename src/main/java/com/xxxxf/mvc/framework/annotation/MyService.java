package com.xxxxf.mvc.framework.annotation;

import java.lang.annotation.*;

/**
 * @author:xxxxf
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyService {
    String value() default "";
}
