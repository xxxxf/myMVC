package com.xxxxf.mvc.framework.annotation;

import java.lang.annotation.*;

/**
 * @author:xxxxf
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyResponseBody{

}
