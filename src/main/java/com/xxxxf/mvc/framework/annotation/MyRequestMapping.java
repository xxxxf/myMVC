package com.xxxxf.mvc.framework.annotation;

import java.lang.annotation.*;

/**
 * @author:xxxxf
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRequestMapping {
    String value() default "";
}
